# Du pad de traduction au Framablog


Les principaux points à connaître et à ne pas oublier pour passer du pad de traduction au Framablog.

Ce n'est pas un tuto pour utiliser Wordpress… mais un guide pour les débutants en « bloguification ».

Pour compléter vos connaissances, voyez notre documentation sur [comment participer au Framablog](/fr/framablog/index.html)

### Phase 1 - finir le travail sur le pad

1. En haut du pad : informer les traducteurs que le passage sur le blog est en cours et que les modifications sur le pad ne seront plus prises en compte
2. Ajouter dans le pad les noms des contributeurs framalang en allant les chercher dans le Timeslider (icône carrée d'horloge dans la barre de menu) 
3. Finir la traduction : choisir une version parmi des suggestions et donc éliminer les autres. Si besoin, réécrire les paragraphes fortement modifiés sous la traduction en cours
4. Exporter le fichier *au format Plain text* en local sur son ordinateur ==> avec l'icône double flèche d'import/export
![exporter en simple texte](images/enregistrer-simple-texte.png)

C'est fini sur le pad… on y trouve si besoin tout l'historique de traduction, le texte y reste dans les deux langues.

### Phase 2 - mettre la traduction sur le blog

1. Se connecter au Framablog, à l'adresse qui vous a été donnée
2. Dans la colonne de gauche du tableau de bord, choisir Articles > Ajouter
3. Indiquer dans le champ « Titre » un titre quelconque qui pourra être modifié ensuite. Ce sera le titre de l'article qui apparaîtra sur le Framablog. En principe ce titre est différent de celui de l'article traduit
4. Coller en mode texte, le texte précédemment exporté du blog. Pour coller en mode texte : cliquer sur "modifier" puis "coller en texte"
5. Modifier le nom de l'auteur de l'article en le passant de l'auteur par défaut à "Framalang". 
	- Tout en haut, à droite de l'écran, cliquer sur "option de l'écran"
	- cocher la case "auteur"
	- aller tout en bas de l'écran, dans le menu déroulant "auteur", choisir "Framalang"
6. Cliquer sur "enregistrer le brouillon" (et pas sur "publier", on en est encore très loin). On peut sauvegarder autant que l'on veut. Wordpress garde la trace des différentes révisions.
7. Vérifier que les boutons "publier sur Twitter" et "publier sur Diaspora" sont cochés.
![vue de la fenêtre d'édition du blog](images/edition-partie-haute.png)


### Phase 3 - finir la traduction

1. Relire attentivement le texte et supprimer au fur et à mesure les parties en anglais. On ne doit pas "sentir" qu'il s'agit d'une traduction. Dit comme ça, ça a l'air simple… mais c'est en général, l'occasion d'une bonne prise de tête ;-) 
2. Faire correspondre la mise en page de la traduction avec le texte original : titre, citation, liste, encart, etc. qui n'apparaissent pas forcément sur le pad.
	- le titre de la traduction est en "titre2"
	- les sous-titres en "titre3", etc.
	- garder les mêmes règles de surlignage, et italique sur l'ensemble du texte.
3. Ajouter les hyperliens
4. Ajouter les illustrations du texte d'origine s'il y en a (voir ci-dessous).

### Phase 4 - rédiger l'article pour le Framablog

La traduction n'est qu'une partie de l'article de blog…

1. Ajouter les contributeurs 
2. Citer la source de l'article avec lien vers l'article original
3. Citer l'auteur + lien vers sa bio ou son twitter
4. Écrire le chapeau de l'article : c'est ce qui apparaît sur la page d'accueil du framablog.
5. Trouver et insérer au moins une image pour illustrer l'article, pour cela :  
	a. trouver une image qui va bien, il faut qu'elle soit en licence libre et qu'elle soit créditée en légende. Pour trouver des photos en Creative Commons, on peut aller [sur ce site](https://search.creativecommons.org/)  
	b. enregistrer l'image en local  
	c. dans le texte du blog, positionner le curseur à l'endroit voulu et faire "add media"  
    d. cliquer sur l'onglet "envoyer un fichier"  
	e. choisir l'image sauvegardée et valider  
	f. cliquer sur "Insérer une image"  
	g. hop l'image est dans le texte, on peut la positionner (gauche/centre/droit) en cliquant une fois dessus des petites icônes apparaissent. Généralement on centre (préférer la taille "grande" ou originale", notre thème se chargera de l'adapter à l'écran), sauf pour l'image de l'auteur qui est plutôt à gauche. En cliquant une fois sur l'image apparaissent les icônes de position et le petit crayon qui sert à ajouter une légende (crédits photos en général)

### Phase 5 - c'est presque fini

1. Relire une dernière fois le texte en vérifiant grammaire, orthographe et typographie. *Perso, je le fais à chaque étape et à chaque fois il en reste… J'ai testé Grammalecte, ça a l'air bien.*
2. Quand c'est fini, informer Goofy pour relecture extérieure et publication.
