# Les outils de traduction

> à compléter au fur et à mesure de la découverte de nouvelles ressources

## Dictionnaires et sites bilingues

- [WordReference](http://www.wordreference.com/) : un bon dictionnaire en ligne qui propose des variantes contextualisées des traductions et des expressions courantes avec le mot recherché. Il est possible de l'ajouter aux moteurs de recherche par défaut de Firefox (comment ?). 
- Le [Forum de WordReference](https://forum.wordreference.com/#french.25) : intéressant pour les tournures idiomatiques, permet d'avoir l'avis d'autres traducteurs sur une expression, une tournure de phrase.
- [Linguee](http://www.linguee.fr/) : permet de rechercher des portions de phrase. Très pratique pour les recherches en contexte.  Il est possible de l'ajouter aux moteurs de recherche par défaut de Firefox. 
- [Le grand dico des Québécois](http://www.granddictionnaire.com/) est très utile pour la traduction des termes techniques. Attention toutefois c'est du français de Québec ;-)
- Également du Canada, on peut consulter la [Banque de dépannage linguistique](http://www.oqlf.gouv.qc.ca/ressources/bdl.html) qui permet de traquer les anglicismes.


## Dictionnaires et sites de langue française

- pour trouver le synonyme d'un mot, deux ressources, le [CRISCO](http://www.crisco.unicaen.fr/des/) de l'université de Caen, et celui du [CNRTL](http://www.cnrtl.fr/synonymie/). Ils s'appuient sur la même base de données, le second fournit en général plus d'occurrences (voire trop)
- pour trouver les co-occurrences d'un mot, c'est-à-dire quel mot est couramment employé avec un autre, c'est dans le [dictionnaire des co-occurrence](http://www.btb.termiumplus.gc.ca/tpv2guides/guides/cooc/index-eng.html?lang=eng) et c'est une initiative des canadiens.
- pour trouver des équivalents français (de France) aux anglicismes et des précisions sur des points de grammaire, [il y a cet outil](http://academie-francaise.fr/questions-de-langue)
- pour trouver les définitions d'un terme particulier ou sa traduction officielle en France (selon le journal officiel), [il faut aller là](http://www.culture.fr/franceterme)

## autres ressources

- [Wikipédia](https://en.wikipedia.org/wiki/Main_Page) peut être une source intéressante pour traduire le nom des organismes et institutions étrangères ainsi que certains termes techniques.
- Les grandes institutions du Libre et/ou d'internet ont en général une version française de leur site. C'est le cas, par exemple, de&nbsp;
 - [GNU](https://www.gnu.org/home.fr.html)
 - [Mozilla](https://www.mozilla.org/fr/)
 - > à compléter au fil des traductions
- Le monde diplomatique a mis en ligne sur [typo.mondediplo.net](http://typo.mondediplo.net/) des informations intéressantes en particulier pour traduire les noms de pays et franciser l'écriture de noms étrangers.


