# Mettre en page un article sur le <b class="violet">Frama</b><b class="rouge">blog</b>

Avant toute chose, il vous faudra vous connecter au <b class="violet">Frama</b><b class="rouge">blog</b> selon l'URL de connexion et les identifiants que nous vous aurons transmis.

## Le fonctionnement de Wordpress&nbsp;:

### Le cadre "publier/planifier" en haut à droite...

* Par défaut vous ne touchez JAMAIS au bouton vert `Publier`, sauf si vous voulez publier l'article&nbsp;;
* On utilise le bouton `Enregistrer comme brouillon`, autant de fois qu'on veut, aucun soucis, ça enregistre vos modifs avec différentiel, c'est beau la vie.
* Exception&nbsp;: lorsqu'on veut planifier un article, on choisit la date & heure, on clique sur `OK` juste à côté. Là, le bouton vert  `Publier` devient `Planifier`&nbsp;: on peut cliquer dessus&nbsp;;
* Dès qu'un article est publié ou planifié, le bouton vert devient `Mettre à jour`. Le seul moyen de savoir si l'article est publié ou juste planifié, c'est de regarder l'état de l'article (deux lignes au dessus du bouton vert)&nbsp;;
* **Toute publication se fait après un accord commun&nbsp;;)**.

### Les copier/coller de la mooooort....

* Wordpress est capricieux&nbsp;: **pas de copier-coller de texte sans avoir coché le bouton  «&nbsp;Coller en texte&nbsp;» auparavant**. Sinon il te chope les balises de LibO/Word/etc et ça pourrit le thème et c'est moche&nbsp;;
* Tu ne trouves pas ce bouton...? c'est normal, tout va bien&nbsp;: il faut activer la deuxième ligne de boutons en cliquant sur  «&nbsp;ouvrir/fermer la barre d'outil&nbsp;». On le fait une fois après on oublie ça reste comme ça&nbsp;!&nbsp;;
* Tu veux copier/insérer du html&nbsp;? OK&nbsp;! Il suffit juste de passer l'éditeur WYSIWYG (onglet «&nbsp;visuel&nbsp;» en haut à droite de l'article) en mode «&nbsp;texte&nbsp;» (par défaut, on est en mode visuel)&nbsp;;
  * Les `div` et les `span` sont à proscrire&nbsp;: on a un thème, merci de ne pas passer par dessus (après, si c'est pour faire un joli cadre avec nos CSS Bootstrap, exceptionnellement, ça se discute, hein&nbsp;: à vous de gérer)&nbsp;;
* Comment je sais si tout va bien&nbsp;: regarder les sauts de ligne (dans l'éditeur WYSIWYG & la prévisualisation de l'article)&nbsp;:
  * S'il y a une petite interligne entre deux paragraphes, tout va bien&nbsp;;
  * S'ils sont collés-serrés, y'a un problème et la lecture de l'article sera étouffante (les sauts de lignes deviennent des retours de chariot&nbsp;: pas glop)&nbsp;;
  * En plus, si tu veux passer un paragraphe en citation ou en titre (2, 3, 4...) ça appliquera la balise à tout l'article (la première fois ça fait drôle)&nbsp;;
* Donc de manière générale&nbsp;: on passe son texte par gedit/bloc-notes/etc. avant de le copier coller&nbsp;: ça sauve des chatons&nbsp;!


### Comment Wordpress a été pensé

Il a été pensé pour être votre éditeur de base&nbsp;: vous êtes sensé·e·s y écrire vos articles directement sur l'interface, sans avoir besoin de rien d'autre. 

Du coup, pour les rebelles comme nous, y'a deux écoles&nbsp;:

* Je fais mon texte en .txt (tout retour à la ligne doit avoir une ligne vide entre 2 paragraphes), je «&nbsp;colle en texte&nbsp;» *dans l'onglet visuel*, j'ajoute liens, images, mise en page (gras, quotes, listes....), readmore, catégories, tags, tweet auto, image à la une => je propose à la publication&nbsp;;
* Je fais mon texte en .html, je copie *dans l'onglet texte*, j'ajoute les images, readmore, catégories, tags, tweet auto, image à la une => je propose à la publication.



## Mise en forme de l'article

<div class="alert alert-warning">
	<ul>
		<li>Wordpress ajoute automatiquement les insécables, guillemets français, ellipsis, parce qu'on a une super extention pour faire le boulot à notre place&nbsp;;)&nbsp;;</li>
		<li><strong>TOUJOURS</strong> penser à mettre une balise «&nbsp;read more&nbsp;» après les deux ou trois premières phrases&nbsp;;</li>
		<li>Ne <strong>JAMAIS</strong> faire de copier-coller depuis word ou writer (ça fait plein de <span> tout moches). Utiliser du texte brut ou le bouton «&nbsp;coller en texte brut&nbsp;» dans la seconde ligne des outils de mise en page.</li>
	</ul>
</div>

### Titres

* Le seul «&nbsp;Titre 1&nbsp;» est le titer de l'article&nbsp;;
* On commence donc notre titrage à partir des titres de niveau 2.

### Catégories

* Toujours utiliser au moins une catégorie&nbsp;;
* On peut multi-catégoriser (à vous de voir ^^)&nbsp;;
* La catégorie par défaut `non classés`, doit donc être vide&nbsp;;
* Toujours cocher la catégorie parente lorsqu'on veut cocher une sous-catégorie.

### Tags

* `RezoTic` et `Planet` permettent d'inclure l'article dans le flux RSS de ces blogs&nbsp;;
* Penser à regarder les `tags les plus utilisés`&nbsp;;
* Utiliser l'auto-complétion&nbsp;;
* Écrire directement un nom de tag dans ce champ (séparer à l'aide de virgules)&nbsp;;
* Valider avec OK&nbsp;;
* Ne pas hésiter à taguer en doublon des catégories/sous catégories (mettre `dégooglisons` ou `GAFAM` dans les tags même si les catégories éponymes sont déjà cochées.

### Publication automatique sur les réseaux

Nous avons installé des plug-in Wordpress pour publier automatiquement les articles sur nos comptes de réseaux sociaux.

* Seuls les nouveaux articles sont publiés, pas leur mises à jour&nbsp;;
* Pas besoin d'avoir les clés du comptes, Wordpress les a.

#### Wordpress to Diaspora*

* Si vous avez correctement mis vos tags wordpress il n'y a RIEN A FAIRE \o/&nbsp;;
* Le plug-in affichera sur Diaspora* l'image à la une de l'article et l'extrait (ce qu'il y a avant la balise Read More) d'où l'importance de ces réglages dans votre article&nbsp;;
* Les tags #framasoft, #framablog, #Libre, #free sont automatiquement ajoutés à votre post Diaspora*&nbsp;;
* le lien vers l'article est ajouté automatiquement.

#### Wordpres to twitter

* Écrivez votre tweet dans le cadre de la colonne de droite prévu à cet effet&nbsp;;
* Ajoutez les tags et mentions que vous voulez dans la limite des 140 caractères&nbsp;;
* Rajoutez IMPÉRATIVEMENT #url# pour inclure le lien vers l'article.

### Illustrations

* Toujours mettre une «&nbsp;Image à la une&nbsp;» (bas de la colonne de droite) afin que le thème l'utilise dans notre page d'accueil, et pour les réseaux sociaux&nbsp;;
* Utiliser des images sous licence libre ou de libre diffusion (chercher sur [Search CreativeCommons](https://ccsearch.creativecommons.org/))&nbsp;;
* Lors de l'ajout d'une image à un article, mettre en légende sous le format `Titre-Licence-Auteur`. Exemple&nbsp;: `Photo magnifique CC-BY-SA Auteur qui déchire`.

### Utilisateur·trice

L'article vous est attribué par défaut. Il faut donc que vous pensiez à aller dans votre profil utilisateur pour&nbsp;:

* Mettre à jour votre mini-bio&nbsp;;
* Mettre vos réseaux sociaux&nbsp;;
* Le champ `facebook` est un lien vers votre profil diaspora*/framasphère (oui on l'a hacké ^^)&nbsp;;
* Pensez à mettre une photo de profil (ou image, dessin, icône, etc.).

