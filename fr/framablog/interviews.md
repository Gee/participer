# Interviewer des initiatives libres

Le but est de mettre en valeur, sans (trop) de complaisance, les personnes et les projets qui mettent plus de Libre dans la vie de chacun·e.

Nous croyons qu'il est possible de partager des initiatives que l'on affectionne sans passer la brosse à reluire, que l'on peut poser les questions qui piquent sans faire un <i lang="en">clash</i> stérile et gratuit, bref&nbsp;: de parler et de faire parler avec sincérité de ce qu'est un projet, et de ce qu'il n'est pas.

## Quelles initiatives proposer&nbsp;?

Alors oui, il est tentant de parler de la valeureuse équipe qui travaille derrière ce logiciel qu'on aime tant (et [nous sommes friand·e·s de ce genre d'entretiens](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2017/01/27/opensuse-une-distribution-meconnue/)).

Néanmoins, nous vous rappelons aussi la devise du <b class="violet">Frama</b><b class="rouge">blog</b>&nbsp;:

> &hellip; mais ce serait peut-être l'une des plus grandes opportunités manquées de notre époque si le logiciel libre ne libérait rien d'autre que du code.

Voici donc quelques exemples concrets et variés d'initiatives dont nous avons aimé parler&nbsp;:

  * Le site de [Co-voiturage Libre](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2017/06/29/pour-un-covoiturage-libre-sans-blabla-car-cest-un-bien-commun/) qui se veut un commun&nbsp;;
  * Sortie du livre [La face cachée d'Internet](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2017/06/20/la-face-cachee-du-web-de-la-vie-de-lunivers-du-reste/), de Rayna Stamboliyska, sur les fantasmes (et réalités) autour du dark ouèbe&nbsp;;
  * Le [projet Arcadie](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2017/07/18/politique-pour-les-arcanes-cest-arcadie/), portail des données sur nos élu·e·s&nbsp;;
  * David Revoy, auteur et illustrateur, propose [sa BD Pepper & Carrot](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2015/12/25/le-poivre-la-carotte-et-la-sorciere-une-interview-de-david-revoy/) sous licence CC-BY&nbsp;;
  * Nathanaël Leprette [dégooglise sa famille](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2015/07/12/aujourdhui-je-degooglise-ma-famille/)&nbsp;;
  * Le média NextINpact [fait le bilan](https://<b class="violet">Frama</b><b class="rouge">blog</b>.org/2017/02/09/next-inpact-bilan-apres-un-an-de-respect-pour-nos-donnees/) après un an de respect pour nos données.

## Comment proposer/réaliser une interview&nbsp;?

Vous avez repéré une intiative qui met plus de libre dans la vie et que vous voulez partager&nbsp;? Parfait&nbsp;: [contactez-nous](https://contact.framasoft.org/#<b class="violet">Frama</b><b class="rouge">blog</b>) en nous la présentant (avec un ou deux liens utiles, histoire qu'on voit ce dont il s'agit). Nous resterons, par la suite, en contact constant par email afin de collaborer ensemble sur votre entretien.

Une fois que nous nous sommes mis·es d'accord sur cette proposition, vous pourrez contacter les personnes derrière cette initiative pour voir si cela les intéresse d'en parler sur le <b class="violet">Frama</b><b class="rouge">blog</b>.

Nous travaillons ensuite de manière collaborative, en utilisant <a href="https://mypads.framapad.org">un dossier <b class="violet">My</b><b class="vert">Pads</b></a> partagé. Pour y avoir accès, il vous faut&nbsp;: 
* Vous créer un compte <b class="violet">My</b><b class="vert">Pads</b> (si ce n'est pas déjà fait)&nbsp;;
* Nous faire un email précisant votre identifiant et/ou votre email d'inscription à MyPads&nbsp;;
* Dès qu'on vous aura rajouté·e au dossier, celui-ci apparaîtra directement dans votre compte <b class="violet">My</b><b class="vert">Pads</b>.

Ensuite, vous créez votre pad de questions qui doit comporter&nbsp;:

1. Un titre du type «&nbsp;Interview de XXX pour parler de YYY&nbsp;»&nbsp;;
2. Un ou deux liens utiles concernant l'initiative que vous voulez partager sur le <b class="violet">Frama</b><b class="rouge">blog</b>&nbsp;;
3. Vos questions en gras, avec de l'espace en dessous pour les réponses&nbsp;;
4. *(optionnel)* Toutes les consignes que vous jugerez bon d'apporter pour les personnes qui vont y répondre (généralement, on les met en italiques)

Vous pourrez enfin nous partager votre pad rempli de questions afin qu'on y contribue avant que de l'envoyer aux personnes concernées&nbsp;;).

## Quelles questions poser&nbsp;?

Alors là, c'est à vous de voir&nbsp;!

Une bonne astuce cependant&nbsp;: mettez-vous à la place d'un·e de vos proches à qui vous aimeriez parler de cette intiative, mais qui n'y connait rien (ou pas grand chose&hellip;). Comment la lui faire découvrir&nbsp;? Quelles seraient ses questions, ses objections&nbsp;?

Généralement, il y a des questions qui reviennent souvent dans les interviews du <b class="violet">Frama</b><b class="rouge">blog</b>. les voici, dans un ordre approximatif&nbsp;:

* Un *«&nbsp;qui êtes vous / d'où venez-vous&nbsp;?&nbsp;»* qui permet de présenter le, la ou les intervenant·e·s&nbsp;;
* Un *«&nbsp;c'est quoi / à quoi ça sert / à qui ça sert&nbsp;?&nbsp;»* permetant de présenter l'initiative&nbsp;;
* Un *«&nbsp;pouquoi l'avez-vous fait / comment ça vous est venu&nbsp;?&nbsp;»* pour comprendre la démarche et/ou les valeurs derrière ce proje&nbsp;;
* Un *«&nbsp;quand est-ce que ça se passe&nbsp;?&nbsp;»* si l'interview est liée à un événement (crowdfunding, dédicace, rencontre, etc.)&nbsp;;
* Un *«&nbsp;où le trouve-t-on / vous trouve-t-on / vous aide-t-on&nbsp;»* lorsqu'on a mieux compris ce dont il s'agit et si l'on veut aller plus loin&nbsp;;
* Un *«&nbsp;comment le faites-vous&nbsp;?&nbsp;»* ou une question technique parce que ça intéressera aussi certaines personnes&nbsp;;
* Une *«&nbsp;question qui pique&nbsp;»* pour parler avec honnêteté de limites du projet ou de choix assumés de l'initiative&nbsp;;
* Un *«&nbsp;mot de la fin&nbsp;»* laissé libre aux personnes interviewées&nbsp;!

## Une fois que vous avez les réponses à vos questions&hellip;

&hellip;ce n'est pas fini&nbsp;!

Il faut encore&nbsp;:
* Écrire un chapô (sur votre pad ou sur [l'interface du blog](mise-en-page.html)), un court texte résument rapidement l'initiative, le la ou les intervenant·e·s, et les raisons qui font que vous vouliez partager ce prjet sur le <b class="violet">Frama</b><b class="rouge">blog</b> (bref&nbsp;: une introduction qui met l'eau à la bouche&nbsp;!)&nbsp;;
* Avoir des illustrations (photos, dessins, captures d'écran, etc.), que vous pouvez demander aux personnes interviewées et introdure dans votre pad à l'aide de liens <b class="violet">Frama</b><b class="vert">pic</b>&nbsp;;
* Finir votre article par un «&nbsp;Pour aller plus loin&nbsp;» avec une liste à puce des liens utiles liés à l'initiative en question&nbsp;;
* Le [mettre en page sur le blog](mise-en-page.html).
