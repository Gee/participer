# Liens à retenir / transmettre

<div class="well">
<p>Les boutons présentés ici vous mènent vers des sites extérieurs à cette documentation. Si vous désirez néanmoins conserver ce site dans un onglet, faites un <strong>clic droit + «&nbsp;ouvrir dans un nouvel onglet&nbsp;»</strong></p>
</div>

## <b class="violet">Frama</b><b class="orange">soft</b>

<div class="clearfix">
	<div class="col-md-3 col-sm-6">
		<a href="https://framasoft.org/" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-home" aria-hidden="true"></p>
		<p><b class="violet">Frama</b><b class="orange">soft</b></p>
		<p><b>Notre site</b></p>
		</a>
	</div>
	<div class="col-md-3 col-sm-6">
		<a href="https://framablog.org" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-pencil" aria-hidden="true"></p>
		<p><b class="violet">Frama</b><b class="rouge">blog</b></p>
		<p><b>Notre blog</b></p>
		</a>
	</div>
	<div class="col-md-3 col-sm-6">
		<a href="https://contact.framasoft.org" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-envelope" aria-hidden="true"></p>
		<p><b class="violet">Nous</b> <b class="orange">écrire</b></p>
		<p><b>Aide & échanges</b></p>
		</a>
	</div>
	<div class="col-md-3 col-sm-6">
		<a href="https://soutenir.framasoft.org" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-graduation-cap" aria-hidden="true"></p>
		<p><b class="violet">Nous</b> <b class="orange">soutenir</b></p>
		<p><b>Faire un don</b></p>
		</a>
	</div>
</div>



## <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

<div class="clearfix">
	<div class="col-md-4 col-sm-6">
		<a href="https://degooglisons-internet.org/" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-shield" aria-hidden="true"></p>
		<p><b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b></p>
		<p><b>Le site</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://degooglisons-internet.org/liste" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-mouse-pointer" aria-hidden="true"></p>
		<p><b class="violet">Liste</b> des <b class="orange">services</b></p>
		<p><b>Nos services</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://docs.framasoft.org" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-graduation-cap" aria-hidden="true"></p>
		<p><b class="violet">Manuels</b> des <b class="orange">services</b></p>
		<p><b>Comment les utiliser ?</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://framasoft.org/nav/html/cgu.html" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-gavel" aria-hidden="true"></p>
		<p><b class="violet">Conidtions</b>&nbsp;<b class="orange">d'utilisation</b></p>
		<p><b>À lire en 3 mn</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://framasoft.org/nav/html/charte.html" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-heart" aria-hidden="true"></p>
		<p><b class="violet">Notre</b> <b class="orange">charte</b></p>
		<p><b>Notre éthique</b></p>
		</a>
	</div>
	<div class="col-md-4 col-sm-6">
		<a href="https://docs.framasoft.org" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-heartbeat" aria-hidden="true"></p>
		<p><b class="violet">État</b> des <b class="orange">services</b></p>
		<p><b>Ça a planté ?</b></p>
		</a>
	</div>
</div>

## Il n'y a pas que <b class="violet">Frama</b><b class="orange">soft</b>&hellip;

<div class="clearfix">
	<div class="col-md-6 col-sm-6">
		<a href="https://degooglisons-internet.org/alternatives" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-recycle" aria-hidden="true"></p>
		<p><b class="violet">Liste</b> des <b class="orange">alternatives</b></p>
		<p><b>Remplacer par quoi ?</b></p>
		</a>
	</div>
	<div class="col-md-6 col-sm-6">
		<a href="https://chatons.org/find" class="btn btn-lg btn-block btn-default">
		<p><i class="fa fa-2x fa-paw" aria-hidden="true"></p>
		<p>Trouver des <b class="orange">CHATONS</b></p>
		<p><b>Services éthiques et locaux</b></p>
		</a>
	</div>
</div>
