# Flyers, affiches et Stickers

<div class="well">
	<strong>Attention&nbsp;:</strong>
	<p>Vous trouverez sur cette page les informations, rendus et fichiers sources pour reproduire et/ou adapter nos outils de communication <i lang="en">print</i> autour du projet <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>.</p>
	<p>Ces données sont partagées afin que vous puissiez vous-même produire ces documents, en toute autonomie.</p>
	<p>Il nous arrive d'aider des initiatives en envoyant nous-mêmes de tels documents à distribuer. Néanmoins, l'immense majorité de nos ressources provenant <a href="https://soutenir.framasoft.org">des dons qui nous sont confiés</a>, nous ne faisons ces envois qu'avec parcimonie et si cela nous semble justifié.</p>
	<p>Si vous désirez recevoir des flyers, etc. merci de <a href="https://contact.framasoft.org">nous faire une demande ici</a> en précisant&nbsp;:</p>
	<ul>
		<li>Votre adresse postale&nbsp;;</li>
		<li>Le(s) type(s) de documents souhaités&nbsp;;</li>
		<li>Le nombre de documents / de personnes à qui vous pensez les distribuer.</li>
	</ul>
	<p>Nous ne pourrons pas répondre favorablement à toute demande, mais nous ferons de notre mieux pour voir cela avec vous&nbsp;!</p>
</div>

## [Flyer] présentation de <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

### Format

  * A6 (10,5 x 14,8 cm), recto-verso, couleurs 
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: Lato

### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-6">
        <a href="images/flyerdio-recto-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/dio-flyer-recto-v2.png" /></a>
        <p class="text-center">Recto</p>
    </div>
    <div class="col-sm-6">
        <a href="images/flyerdio-verso-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/dio-flyer-verso-v2.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>



## [Dépliant] Le projet <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

### Format

  * 10 x 21 cm fermé, A4 paysage (21 x 29,7 cm) ouvert, 2 plis roulés, 3 volets, recto-verso, couleurs 
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: Lato, DejaVu Sans


### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/depliantdio-gab-recto-201705-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/depliantdio-gab-recto-201705-debord5mm.png" /></a>
        <p class="text-center">Recto</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="images/depliantdio-gab-recto-201705-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/depliant-dio-gab-verso-201705-debord5mm.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>


## [Dépliant] Les services <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

### Format

  * A5 (14,8 x 21 cm) fermé, A4 paysage (21 x 29,7 cm) ouvert, 1 pli, 2 volets, recto-verso, couleurs 
  * Débord (bord de coupe)&nbsp;: 5 mm
  * Fond perdu (zone de sécurité)&nbsp;: 3 mm
  * Police d'écriture utilisée&nbsp;: Lato, DejaVu Sans


### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/dioA4-exterieur-201705-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/dioA4-exterieur-201705-debord5mm.png" /></a>
        <p class="text-center">Recto</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <a href="images/dioA4-interieur-201705-debord5mm.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/dioA4-interieur-201705-debord5mm.png" /></a>
        <p class="text-center">Verso</p>
    </div>
</div>

## [Stickers] «&nbsp;GAFAM, we <3 your Data&nbsp;»

### Format

  * 8 x 6 cm, paysage (sauf *Apple*&nbsp;: portrait), recto, couleurs 
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Polices d'écritures utilisées&nbsp;: Permanent Marker (*GAFAM*), Sans (*Google*, *Facebook*), Lato (*Amazon*), Aller Display (*Apple*), Axure Handwriting (*Microsoft*). 

### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-4">
        <a href="images/stickerDiO_gafam.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/stickerDiO_gafam.png" /></a>
        <p class="text-center"><i lang="en">GAFAM, we &lt;3 your data!</i></p>
    </div>
    <div class="col-sm-4">
        <a href="images/stickerDiO_google.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/stickerDiO_google.png" /></a>
        <p class="text-center"><i lang="en">Warning: do not feed the Google.</i></p>
    </div>
    <div class="col-sm-4">
        <a href="images/stickerDiO_amazon.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/stickerDiO_amazon.png" /></a>
        <p class="text-center">Attention&nbsp;: Amazon très méchant</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <a href="images/stickerDiO_facebook.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/stickerDiO_facebook.png" /></a>
        <p class="text-center"><i lang="en">Facebook is watching you.</i></p>
    </div>
    <div class="col-sm-4">
        <a href="images/stickerDiO_apple.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/stickerDiO_apple.png" /></a>
        <p class="text-center"><i lang="en">Apple: Kids, don't do drugs.</i></p>
    </div>
    <div class="col-sm-4">
        <a href="images/stickerDiO_microsoft.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
             src="images/stickerDiO_microsoft.png" /></a>
        <p class="text-center"><i lang="en">Microsoft: do you need a backdoor?</i></p>
    </div>
</div>

## [Affichettes] «&nbsp;GAFAM, we <3 your Data&nbsp;»

### Format

  * A4 (21 x 29,7 cm), paysage (sauf *Apple*&nbsp;: portrait), 6 pages, recto, couleurs 
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm

### Sources

<div class="well">
	<p>Il s'agit des visuels des stickers au format A4 pour l'impression d'affichettes.</p>
</div>
<div class="row">
	<p class="col-md-6 col-md-offset-3 text-center">
    <a class="btn btn-primary btn-lg btn-block" href="images/a4_stickers.pdf"><i class="fa fa-fw fa-file-pdf-o"></i> Téléchager le .pdf<br>des 6 affichettes.</a>
	</p>
</div>

## [Affiche] Carte <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

### Format

  * A3 (29,7 x 42 cm), portrait, recto, couleurs 
  * Fond perdu (zone de sécurité)&nbsp;: 5 mm
  * Police d'écriture utilisée&nbsp;: Lato

### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/dio-poster-2016.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/dio-poster_v20160817.png" /></a>
        <p class="text-center">Poster A3</p>
    </div>
</div>


## [Kakemono] <b class="violet">Dégooglisons</b>&nbsp;<b class="orange">Internet</b>

### Format

  * 85 x 200 cm, portrait, recto, couleurs 
  * Polices d'écritures utilisées&nbsp;: Monospace, Sans, DejaVu Sans

### Le rendu & les sources

<div class="well">
	<strong>Clic droit sur l’image puis&hellip;</strong>
	<ul>
		<li> choisissez <em>«&nbsp;Enregistrer l’image sous&nbsp;»</em> pour la réutiliser ou l'imprimer (fichier .png)</li>
		<li> choissiez <em>«&nbsp;Enregister la cible du lien sous&nbsp;»</em> pour avoir directement accès à la source (fichier .svg inkscape)</li>
	</ul>
	<p>Ce site adapte la taille des images à votre écran, mais les fichiers que vous enregistrerez ainsi seront du bon format, utilisables directement auprès de votre imprimeur·euse (format .png) ou de votre graphiste (format .svg).</p>
</div>

<div class="row">
    <div class="col-sm-12">
        <a href="images/kakemono.svg"><img class="img-responsive" data-toggle="tooltip" data-placement="top"
            title="Clic droit sur l’image et «&nbsp;Enregistrer l’image sous&nbsp;» pour la réutiliser" alt=""
            src="images/kakemono.png" /></a>
        <p class="text-center">Kakemono</p>
    </div>
</div>


